package com.nikita.demo.repositories;


import com.nikita.demo.univer.Subject;
import org.springframework.data.repository.CrudRepository;

public interface SubjectRepository extends CrudRepository<Subject, Integer> {
    Subject findByName(String name);
}
