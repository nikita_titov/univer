package com.nikita.demo.repositories;


import com.nikita.demo.univer.UniversityGroup;
import org.springframework.data.repository.CrudRepository;

public interface UniversityGroupRepository extends CrudRepository<UniversityGroup, Integer> {
    UniversityGroup findByGroup(Integer group);
}
